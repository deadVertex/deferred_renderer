uniform mat4 combinedMatrix;
uniform mat4 modelMatrix;
uniform float time;

interface VSOutput
{
  vec2 textureCoordinates;
};

vec3 CalculateDominantAxis( vec3 n )
{
  vec3 v;
  float m = max( abs( n.x ), max( abs( n.y ), abs( n.z ) ) );
  // Multiply by sign to convert to box projection.
  v.x = abs( n.x ) < m ? 0.0 : 1.0;
  v.y = abs( n.y ) < m ? 0.0 : 1.0;
  v.z = abs( n.z ) < m ? 0.0 : 1.0;
  return v;
}

vec2 ConvertWorldPositionToTextureCoordinate( vec3 normal, vec3 position )
{
	vec3 n = CalculateDominantAxis( normal );
	if ( n.x > 0.0 )
		return vec2( position.y, position.z );
	else if ( n.y > 0.0 )
		return vec2( position.x, position.z );
	else if ( n.z > 0.0 )
		return vec2( position.x, position.y );
	return vec2( 0.0, 0.0 );
}

shader VS_MainFogShader( in vec3 position : 0, in vec3 normal : 1,
                         out VSOutput output )
{
  gl_Position = combinedMatrix * vec4( position, 1.0 );
  vec3 worldPosition = vec3( modelMatrix * vec4( position, 1.0 ) );
  vec3 n =
    vec3( transpose( inverse( modelMatrix ) ) * vec4( normal, 0.0 ) );
  worldPosition += vec3( -0.1, 0.4, 0.2 ) * time;
  output.textureCoordinates =
    ConvertWorldPositionToTextureCoordinate( n, worldPosition );
  //output.textureCoordinates += vec2( -0.1, 0.4 ) * time;
}

uniform sampler2D depthTexture;
uniform sampler2D sceneDepthTexture;
uniform vec2 screenSize;
uniform vec3 cameraPosition;
uniform mat4 invViewProjection;
uniform vec3 volumeColour;
uniform float volumeDensity;
uniform sampler2D colourTexture;


float LinearizeDepth( float z, float near, float far )
{
  return ( 2.0 * near ) / ( far + near - z * ( far - near ) );
}

vec2 CalculateTextureCoordinates()
{
  return gl_FragCoord.xy / screenSize;
}

vec3 CalculateWorldPosition( vec2 position, float depth )
{
  vec3 worldPosition = vec3( position, depth ) * 2.0 - 1.0;
  vec4 temp = invViewProjection * vec4( worldPosition, 1.0 );
  worldPosition = temp.xyz / temp.w;
  return worldPosition;
}

shader FS_MainFogShader( in VSOutput input, out vec4 output )
{
  vec2 textureCoordinates = CalculateTextureCoordinates();
  float zFront = texture( depthTexture, textureCoordinates ).r;
  float zBack = gl_FragCoord.z;
  float zScene = texture( sceneDepthTexture, textureCoordinates ).r;
  if ( zFront == 1.0 )
  {
    if ( zBack > zScene )
    {
      zBack = zScene;
    }
  }
  else
  {
    if ( zBack > zScene )
    {
      if ( zFront  > zScene )
      {
        discard;
      }
      zBack = zScene;
    }
  }
  vec3 front = CalculateWorldPosition( textureCoordinates, zFront );
  vec3 back = CalculateWorldPosition( textureCoordinates, zBack );
  vec3 V = normalize( cameraPosition - back );
  vec3 N = vec3( 0, 1, 0 );
  vec3 R = normalize( 2 * dot( N, V ) * N - V );
  /*vec2 tc = ConvertWorldPositionToTextureCoordinate( R, cameraPosition );*/
  float density = texture( colourTexture, input.textureCoordinates * 0.08 ).r;
  float d = min( length( front - back ), length( cameraPosition - back ) );
  float f = exp( -d * volumeDensity );
  output = vec4( volumeColour, 1 - f );
}

program FogShader
{
  vs(330)=VS_MainFogShader();
  fs(330)=FS_MainFogShader();
};

shader FS_MainDepthPass( out vec4 output )
{
  output = vec4( 1.0 );
}

program DepthPass
{
  vs(330)=VS_MainFogShader();
  fs(330)=FS_MainDepthPass();
};
