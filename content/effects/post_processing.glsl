interface VS_OutputPostProcessing
{
  vec2 textureCoordinates;
};

shader VS_MainPostProcessing( in vec3 position : 0, in vec2 texCoord : 2,
                       out VS_OutputPostProcessing output )
{
  output.textureCoordinates = texCoord;
  gl_Position = vec4( position, 1.0 );
}

uniform sampler2D hdrTexture;
uniform vec2 screenSize;

float FilmicToneMap( float x )
{
  x = max( 0.0, x - 0.004 );
  return ( x * ( 6.2 * x + 0.5 ) ) / ( x * ( 6.2 * x + 1.7 ) + 0.06 );
}
vec3 ToneMap( vec3 colour )
{
  return vec3( FilmicToneMap( colour.r ), FilmicToneMap( colour.g ),
               FilmicToneMap( colour.b ) );
}

float Luminance( vec3 colour )
{
  return 0.2126 * colour.r + 0.7152 * colour.g + 0.0722 * colour.b;
}

vec3 ToSRGB( vec3 colour )
{
  return vec3( pow( colour.r, 0.45 ), pow( colour.g, 0.45 ),
               pow( colour.b, 0.45 ) );
}

vec3 BrightPass( vec3 colour )
{
  float lum = Luminance( colour );
  float lum2 = max( lum - 0.0, 0.0f );
  return colour * ( lum2 / lum );
}

vec3 SampleTexture( vec2 textureCoordinates, float lod )
{
  return BrightPass( textureLod( hdrTexture, textureCoordinates, lod ).rgb );
}

vec3 CalculateBloom( vec2 tc )
{
  float lod = 2.0;
  float w = screenSize.x;
  const int stepCount = 2;
  float weights[stepCount] = float[](0.44908, 0.05092);
  float offsets[stepCount] = float[](0.53805, 2.06278);

  vec2 pixelOffset = 1.0 / screenSize;

  vec3 result = vec3( 0 );
  for ( int i = 0; i < stepCount; i++ )
  {
    vec2 offset = offsets[i] * pixelOffset;
    vec3 temp =
      SampleTexture( tc + offset, lod ) + SampleTexture( tc - offset, lod );
    result += weights[i] * temp;
  }

  /*vec3 result = SampleTexture( tc, lod ) * 0.204164;*/
  /*result +=*/
    /*SampleTexture( tc + vec2( 1.407333 ) / w, lod ) * 0.304005;*/
  /*result +=*/
    /*SampleTexture( tc - vec2( 1.407333 ) / w, lod ) * 0.304005;*/
  /*result +=*/
    /*SampleTexture( tc + vec2( 3.294215 ) / w, lod ) * 0.093913;*/
  /*result +=*/
    /*SampleTexture( tc - vec2( 3.294215 ) / w, lod ) * 0.093913;*/
  return result;
}

shader FS_MainPostProcessing( in VS_OutputPostProcessing input,
                              out vec4 output )
{
  vec3 hdrColour = texture( hdrTexture, input.textureCoordinates ).rgb;
  vec3 whitePoint = vec3( 1.0 );
  float exposure = 1.0;
  vec3 bloom = CalculateBloom( input.textureCoordinates );
  hdrColour += bloom;
  vec3 ldrColour = ToneMap( hdrColour * exposure ) / ToneMap( whitePoint );
  output = vec4( ldrColour, 1.0 );
}

program PostProcessing_HDR
{
  vs(330)=VS_MainPostProcessing();
  fs(330)=FS_MainPostProcessing();
};
