uniform mat4 modelMatrix = mat4(1);
uniform mat4 combinedMatrix = mat4(1);
uniform vec3 cameraPosition;

interface VS_OutputTexture
{
  vec3 normal, tangent, bitangent;
  vec2 textureCoordinates;
};

shader VS_MainTexture( in vec3 position : 0, in vec3 normal : 1,
                       in vec2 texCoord : 2, in vec3 tangent : 4,
                       in vec3 bitangent : 5, out VS_OutputTexture output )
{
  output.textureCoordinates = texCoord;
  output.normal = ( modelMatrix * vec4( normal, 0.0 ) ).xyz;
  output.tangent = ( modelMatrix * vec4( tangent, 0.0 ) ).xyz;
  output.bitangent = ( modelMatrix * vec4( bitangent, 0.0 ) ).xyz;
  gl_Position = combinedMatrix * vec4( position, 1.0 );
}

uniform sampler2D albedoTexture;
uniform sampler2D normalTexture;

#if 0
// http://www.thetenthplanet.de/archives/1180
mat3 CotangentFrame( vec3 N, vec3 p, vec2 uv )
{
  vec3 dp1 = dFdx( p );
  vec3 dp2 = dFdy( p );
  vec2 duv1 = dFdx( uv );
  vec2 duv2 = dFdy( uv );

  vec3 dp2perp = cross( dp2, N );
  vec3 dp1perp = cross( N, dp1 );
  vec3 T = dp2perp * duv1.x + dp1perp * duv2.x;
  vec3 B = dp2perp * duv1.y + dp1perp * duv2.y;

  float invmax = inversesqrt( max( dot( T, T ), dot( B, B ) ) );
  return mat3( T * invmax, B * invmax, N );
}

vec3 PerturbNormal( vec3 N, vec3 V, vec2 texCoord )
{
  vec3 sample = texture( normalTexture, texCoord ).xyz * 2.0 - 1.0;
  sample = vec3( 0, 0, 1 );
  mat3 TBN = CotangentFrame( N, -V, texCoord );
  return normalize( TBN * sample );
}
#endif

vec3 PerturbNormal( vec3 N, vec3 T, vec3 B, vec2 texCoord )
{
  vec3 sample = texture( normalTexture, texCoord ).rgb * 2.0 - 1.0;
  sample = normalize( sample );
  N = normalize( N );
  T = normalize( T );
  T = normalize( T - dot( T, N ) * N );
  B = normalize( cross( N, T ) );
  mat3 TBN = mat3( T, B, N );
  return normalize( TBN * sample );
}

shader FS_MainTexture( in VS_OutputTexture input, out vec3 albedoOut : 0,
                       out vec3 normalOut : 1 )
{
  albedoOut = texture( albedoTexture, input.textureCoordinates ).rgb;
  normalOut = PerturbNormal( input.normal, input.tangent, input.bitangent,
                             input.textureCoordinates );
}

program GeometryPass_Texture
{
  vs(330)=VS_MainTexture();
  fs(330)=FS_MainTexture();
};

interface VS_OutputColour
{
  vec3 normal;
};

shader VS_MainColour( in vec3 position : 0, in vec3 normal : 1,
                      out VS_OutputColour output )
{
  output.normal =
    vec3( transpose( inverse( modelMatrix ) ) * vec4( normal, 0.0 ) );
  gl_Position = combinedMatrix * vec4( position, 1.0 );
}

uniform vec3 albedoColour;

shader FS_MainColour( in VS_OutputColour input, out vec3 albedoOut : 0,
                      out vec3 normalOut : 1 )
{
  albedoOut = albedoColour;
  normalOut = normalize( input.normal );
}

program GeometryPass_Colour
{
  vs(330)=VS_MainColour();
  fs(330)=FS_MainColour();
};
