#include <cstdio>

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/compatibility.hpp>

#include "opengl_utils.h"
#include "render.h"
#include "resource_utils.h"
#include "csg.h"
#include "utils.h"
#include "imgui.h"
#include "imgui_integration.h"

static bool run = true;
static glm::mat4 projectionMatrix;
static bool enableMouseLook = false;
static int windowWidth, windowHeight;
static bool windowWasResized = true;
static void WindowResizeCallback( GLFWwindow *window, int width, int height )
{
  projectionMatrix = glm::perspective( glm::radians( 80.0f ),
      (float)width / (float)height, 0.1f, 100.0f );
  windowWidth = width;
  windowHeight = height;
  windowWasResized = true;
}

enum
{
  KEY_W = 1,
  KEY_S = 2,
  KEY_A = 4,
  KEY_D = 8
};

uint8_t keyStates = 0;
static void KeyCallback( GLFWwindow* window, int key, int scancode, int action,
                         int mods )
{
  if ( !enableMouseLook )
  {
    uiOnKeyInput( window, key, scancode, action, mods );
  }
  if ( action == GLFW_PRESS )
  {
    switch ( key )
    {
    case GLFW_KEY_W:
      keyStates |= KEY_W;
      break;
    case GLFW_KEY_S:
      keyStates |= KEY_S;
      break;
    case GLFW_KEY_A:
      keyStates |= KEY_A;
      break;
    case GLFW_KEY_D:
      keyStates |= KEY_D;
      break;
    case GLFW_KEY_ESCAPE:
      run = false;
      break;
    default:
      break;
    }
  }
  else if ( action == GLFW_RELEASE )
  {
    switch ( key )
    {
    case GLFW_KEY_W:
      keyStates &= ~KEY_W;
      break;
    case GLFW_KEY_S:
      keyStates &= ~KEY_S;
      break;
    case GLFW_KEY_A:
      keyStates &= ~KEY_A;
      break;
    case GLFW_KEY_D:
      keyStates &= ~KEY_D;
      break;
    default:
      break;
    }
  }
}

void MouseButtonCallback( GLFWwindow *window, int button, int action, int mods )
{
  if ( !enableMouseLook )
  {
    uiOnMouseButtonInput( window, button, action, mods );
  }

  if ( button == GLFW_MOUSE_BUTTON_RIGHT )
  {
    if ( action == GLFW_PRESS )
    {
      glfwSetInputMode( window, GLFW_CURSOR, GLFW_CURSOR_DISABLED );
      enableMouseLook = true;
    }
    else
    {
      glfwSetInputMode( window, GLFW_CURSOR, GLFW_CURSOR_NORMAL );
      enableMouseLook = false;
    }
  }
}

struct Camera
{
  glm::vec3 position, orientation;
  glm::vec3 oldPosition, oldOrientation;
};

static void UpdateCamera( Camera* camera, glm::vec2 rotation,
                          glm::vec3 velocity, float dt )
{
  camera->oldPosition = camera->position;
  camera->oldOrientation = camera->orientation;

  camera->orientation += glm::vec3( rotation, 0 ) * dt;
  camera->orientation.x = glm::clamp(
    camera->orientation.x, -glm::half_pi<float>(), glm::half_pi<float>() );

  glm::mat4 rotationMatrix;
  rotationMatrix =
    glm::rotate( rotationMatrix, camera->orientation.x, glm::vec3{1, 0, 0} );
  rotationMatrix =
    glm::rotate( rotationMatrix, camera->orientation.y, glm::vec3{0, 1, 0} );

  rotationMatrix = glm::inverse( rotationMatrix );
  glm::vec3 forward = glm::vec3( rotationMatrix * glm::vec4{0, 0, 1, 0} );
  forward = glm::normalize( forward );

  glm::vec3 right = glm::vec3( rotationMatrix * glm::vec4{1, 0, 0, 0} );
  right = glm::normalize( right );

  glm::vec3 up = glm::vec3( rotationMatrix * glm::vec4{0, 1, 0, 0} );
  up = glm::normalize( up );

  camera->position += right * velocity.x * dt;
  camera->position += forward * velocity.z * dt;
}

static glm::mat4 CreateViewMatrix( Camera *camera, float interp )
{
  glm::vec3 position =
    glm::lerp( camera->oldPosition, camera->position, interp );
  glm::vec3 orientation =
    glm::lerp( camera->oldOrientation, camera->orientation, interp );

  glm::mat4 viewMatrix;
  viewMatrix =
    glm::rotate( viewMatrix, orientation.x, glm::vec3{1, 0, 0} );
  viewMatrix =
    glm::rotate( viewMatrix, orientation.y, glm::vec3{0, 1, 0} );
  viewMatrix = glm::translate( viewMatrix, -position );
  return viewMatrix;
}

void APIENTRY OpenGLReportErrorMessage( GLenum source, GLenum type, GLuint id,
                                        GLenum severity, GLsizei length,
                                        const GLchar *message,
                                        void *userParam )
{
  const char *typeStr = nullptr;
  switch ( type )
  {
    case GL_DEBUG_TYPE_ERROR:
      typeStr = "ERROR";
      break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
      typeStr = "DEPRECATED_BEHAVIOR";
      break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
      typeStr = "UNDEFINED_BEHAVIOR";
      break;
    case GL_DEBUG_TYPE_PORTABILITY:
      typeStr = "PORTABILITY";
      break;
    case GL_DEBUG_TYPE_PERFORMANCE:
      typeStr = "PERFORMANCE";
      break;
    case GL_DEBUG_TYPE_OTHER:
      typeStr = "OTHER";
      break;
    default:
      typeStr = "";
      break;
  }

  const char *severityStr = nullptr;
  switch ( severity )
  {
    case GL_DEBUG_SEVERITY_LOW:
      severityStr = "LOW";
      break;
    case GL_DEBUG_SEVERITY_MEDIUM:
      severityStr = "MEDIUM";
      break;
    case GL_DEBUG_SEVERITY_HIGH:
      severityStr = "HIGH";
      break;
    default:
      severityStr = "";
      break;
  }

  fprintf( stdout, "OPENGL|%s:%s:%s\n", typeStr, severityStr, message );
}
enum
{
  U_COMBINED_MATRIX,
  U_ALBEDO_TEXTURE,
  U_NORMAL_TEXTURE,
  U_DEPTH_TEXTURE,
  U_INVERSE_VIEW_PROJECTION,
  U_SCREEN_SIZE,
  U_LIGHT_POSITION,
  U_LIGHT_COLOUR,
  U_LIGHT_ATTENUATION,
  U_LIGHT_DIRECTION,
  U_LIGHT_CONE,
  U_HDR_TEXTURE,
  U_CAMERA_POSITION,
  U_INVERSE_MODEL_MATRIX,
  U_SCENE_DEPTH_TEXTURE,
  U_VOLUME_COLOUR,
  U_VOLUME_DENSITY,
  U_COLOUR_TEXTURE,
  U_MODEL_MATRIX,
  U_TIME,
  U_ALBEDO_COLOUR,
  U_SHADOW_MAP,
  U_LIGHT_VIEW_PROJECTION,
  U_SURFACE_TEXTURE,
  MAX_UNIFORM_NAMES,
};

const char *uniformNames[MAX_UNIFORM_NAMES] = {
  "combinedMatrix", "albedoTexture",       "normalTexture",
  "depthTexture",   "invViewProjection",   "screenSize",
  "lightPosition",  "lightColour",         "lightAttenuation",
  "lightDirection", "lightCone",           "hdrTexture",
  "cameraPosition", "invModelMatrix",      "sceneDepthTexture",
  "volumeColour",   "volumeDensity",       "colourTexture",
  "modelMatrix",    "time",                "albedoColour",
  "shadowMap",      "lightViewProjection", "surfaceTexture" };

struct Shader
{
  int program;
  int uniformLocations[MAX_UNIFORM_NAMES];
};

struct PostProcessingShader
{
  int program;
  int hdrTextureLocation, depthTextureLocation;
};

Shader LoadShader( int effect, const char *programName )
{
  Shader result = {};
  result.program = CompileProgram( effect, programName );
  for ( uint32_t i = 0; i < MAX_UNIFORM_NAMES; ++i )
  {
    result.uniformLocations[i] =
      glGetUniformLocation( result.program, uniformNames[i] );
  }
  return result;
}

#define MAX_POINT_LIGHTS 256
#define MAX_SPOT_LIGHTS 256
#define MAX_FOG_VOLUMES 32

struct PointLight
{
  glm::vec3 colour, attenuation, position;
  float radius;
};

struct SpotLight
{
  glm::vec3 colour, attenuation, position, direction;
  float range, innerAngle, outerAngle;
  ShadowBuffer shadowBuffer;
  glm::mat4 viewProjection;
};

struct FogVolume
{
  glm::vec3 colour, position, dimensions;
  float density;
};

struct DirectionalLight
{
  glm::vec3 colour, direction;
  ShadowBuffer shadowBuffer;
  glm::mat4 viewProjection;
};

struct Scene
{
  int geometryPass, lightingPass, postProcessingEffect, fogEffect,
    decalPassEffect;
  OpenGLStaticMesh fullscreenQuad, wireframeCube, axisMesh, groundPlaneMesh,
    cubeMesh, levelGeometryMesh, icosphere, cone, monkeyMesh, sphereMesh;
  uint32_t devTileTexture, csgTexture, fogTexture, bulletHoleTexture,
    normalTexture, bulletHoleNormalTexture;
  CsgSystem *csgSystem;
  Camera camera;
  GBuffer gbuffer;
  HDRBuffer hdrBuffer;
  VolumetricBuffer volumeBuffer;
  Shader pointLightShader, directionalLightShader, spotLightShader, fogShader,
    fogDepthPass, decalPass, geometryPassColour, geometryPassTexture,
    postProcessingShader;
  PointLight pointLights[MAX_POINT_LIGHTS];
  SpotLight spotLights[MAX_SPOT_LIGHTS];
  FogVolume fogVolumes[MAX_FOG_VOLUMES];
  DirectionalLight sun;
  uint32_t numPointLights, numSpotLights, numFogVolumes;
  float currentTime;
};

internal CsgShape* CreateMap2( CsgSystem *csgSystem )
{
  auto outer = CsgCreateBox( csgSystem, glm::vec3{-1.0, -1.0, -1.0},
                             glm::vec3{21, 3, 21} );
  auto inner =
    CsgCreateBox( csgSystem, glm::vec3{-0, 0, -0}, glm::vec3{20, 3, 20} );

  auto col1 =
    CsgCreateBox( csgSystem, glm::vec3{4, 0, 4}, glm::vec3{8, 3, 8} );

  auto col2 =
    CsgCreateBox( csgSystem, glm::vec3{12, 0, 4}, glm::vec3{16, 3, 8} );

  auto col3 =
    CsgCreateBox( csgSystem, glm::vec3{4, 0, 12}, glm::vec3{8, 3, 16} );

  auto col4 =
    CsgCreateBox( csgSystem, glm::vec3{12, 0, 12}, glm::vec3{16, 3, 16} );

  auto level = CsgSubtract( csgSystem, outer, inner );
  level = CsgUnion( csgSystem, level, col1 );
  level = CsgUnion( csgSystem, level, col2 );
  level = CsgUnion( csgSystem, level, col3 );
  level = CsgUnion( csgSystem, level, col4 );
  CsgTranslate( level, glm::vec3{ -2, 0, -2 } );
  return level;
}

internal void GenerateLevel( Scene *scene )
{
  scene->csgSystem = CreateCsgSystem( 1024, 0xFFFF );
  auto level = CreateMap2( scene->csgSystem );
  //auto level = CsgCreateBox( scene->csgSystem, glm::vec3{-1}, glm::vec3{1} );
  PolygonVertex vertices[1024];
  uint32_t numVertices =
    CsgGenerateTriangeMeshData( vertices, 1024, level );

  OpenGLVertexAttribute attribs[5];
  attribs[0].index = VERTEX_ATTRIBUTE_POSITION;
  attribs[0].numComponents = 3;
  attribs[0].componentType = GL_FLOAT;
  attribs[0].normalized = GL_FALSE;
  attribs[0].offset = 0;
  attribs[1].index = VERTEX_ATTRIBUTE_NORMAL;
  attribs[1].numComponents = 3;
  attribs[1].componentType = GL_FLOAT;
  attribs[1].normalized = GL_FALSE;
  attribs[1].offset = offsetof( PolygonVertex, normal );
  attribs[2].index = VERTEX_ATTRIBUTE_TEXTURE_COORDINATE;
  attribs[2].numComponents = 2;
  attribs[2].componentType = GL_FLOAT;
  attribs[2].normalized = GL_FALSE;
  attribs[2].offset = offsetof( PolygonVertex, textureCoordinate );
  attribs[3].index = VERTEX_ATTRIBUTE_TANGENT;
  attribs[3].numComponents = 3;
  attribs[3].componentType = GL_FLOAT;
  attribs[3].normalized = GL_FALSE;
  attribs[3].offset = offsetof( PolygonVertex, tangent );
  attribs[4].index = VERTEX_ATTRIBUTE_BITANGENT;
  attribs[4].numComponents = 3;
  attribs[4].componentType = GL_FLOAT;
  attribs[4].normalized = GL_FALSE;
  attribs[4].offset = offsetof( PolygonVertex, bitangent );
  scene->levelGeometryMesh =
    OpenGLCreateStaticMesh( vertices, numVertices, NULL, 0,
                            sizeof( PolygonVertex ), attribs, 5, GL_TRIANGLES );
}

float CalculateLightMaximumRange( const glm::vec3 &colour,
                                  const glm::vec3 &attenuation )
{
  float intensity = glm::max( glm::max( colour.r, colour.g ), colour.b );

  float a = attenuation.z;
  float b = attenuation.y;
  float c = attenuation.x;
  return ( -b + glm::sqrt( b * b - 4 * a * ( a - 256 * intensity ) ) ) /
         ( 2 * a );
}
void UpdatePointLightMaximumRadius( PointLight *light )
{
  light->radius =
    CalculateLightMaximumRange( light->colour, light->attenuation ) * 1.5;
}

internal void LoadContent( Scene *scene )
{
  scene->geometryPass =
    LoadEffect( "../content/effects/geometry_pass.glsl" );
  scene->geometryPassTexture =
    LoadShader( scene->geometryPass, "GeometryPass_Texture" );
  scene->geometryPassColour =
    LoadShader( scene->geometryPass, "GeometryPass_Colour" );

  scene->lightingPass = LoadEffect( "../content/effects/lighting_pass.glsl" );
  scene->directionalLightShader =
    LoadShader( scene->lightingPass, "LightingPass_Directional" );
  scene->pointLightShader =
    LoadShader( scene->lightingPass, "LightingPass_Point" );
  scene->spotLightShader =
    LoadShader( scene->lightingPass, "LightingPass_Spot" );

  scene->fogEffect = LoadEffect( "../content/effects/fog.glsl" );
  scene->fogDepthPass = LoadShader( scene->fogEffect, "DepthPass" );
  scene->fogShader = LoadShader( scene->fogEffect, "FogShader" );

  scene->postProcessingEffect =
    LoadEffect( "../content/effects/post_processing.glsl" );
  scene->postProcessingShader =
    LoadShader( scene->postProcessingEffect, "PostProcessing_HDR" );

  scene->decalPassEffect = LoadEffect( "../content/effects/decal_pass.glsl" );
  scene->decalPass = LoadShader( scene->decalPassEffect, "DecalPass" );

  scene->fullscreenQuad = CreateFullscreenQuadMesh();
  scene->wireframeCube = CreateWireframeCube();
  scene->axisMesh = CreateAxisMesh();
  scene->groundPlaneMesh = CreatePlaneMesh( 4.0f, 4.0f, 2.0f );
  scene->cubeMesh = CreateCubeMesh();
  scene->monkeyMesh = LoadMesh( "../content/meshes/monkey.obj" );
  scene->sphereMesh = LoadMesh( "../content/meshes/unit_cube.obj" );
  //scene->devTileTexture = LoadTexture( "../content/textures/dev_tile512.mte" );
  scene->csgTexture =
    LoadTexture( "../content/textures/dev_csg1M.png", TEXTURE_MIPMAPS );
  scene->fogTexture =
    LoadTexture( "../content/textures/fog.png", TEXTURE_MIPMAPS );
  scene->normalTexture = LoadTexture( "../content/textures/test_normal_map.png",
                                      TEXTURE_MIPMAPS | TEXTURE_NORMAL_MAP );
  scene->bulletHoleTexture = LoadTexture( "../content/textures/bullethole.png",
                                          TEXTURE_MIPMAPS, PIXEL_FORMAT_RGBA8 );
  scene->bulletHoleNormalTexture =
    LoadTexture( "../content/textures/bullethole_n.png",
                 TEXTURE_MIPMAPS | TEXTURE_NORMAL_MAP );
  scene->icosphere = CreateIcosahedronMesh( 2 );
  scene->cone = CreateCone( 16 );

  GenerateLevel( scene );

  scene->numPointLights = 4;
  for ( uint32_t i = 0; i < scene->numPointLights; ++i )
  {
    auto light = scene->pointLights + i;
    light->colour = glm::vec3{ 0.8, 0.9, 0.8 };
    light->position = glm::vec3{ 0, 0.5, i * 4 };
    light->attenuation = glm::vec3{ 0, 0.5, 2.0 };
    UpdatePointLightMaximumRadius( light );
  }

  scene->numSpotLights = 4;
  for ( uint32_t i = 0; i < scene->numSpotLights; ++i )
  {
    auto light = scene->spotLights + i;
    light->colour = glm::vec3{ 1, 0.9, 0.75 } * 2.0f;
    light->position = glm::vec3{ ( i + 1 ) * 4, 6, 4 };
    light->attenuation = glm::vec3{ 0, 0.0, 0.1 };
    light->direction = glm::normalize( glm::vec3{ 0, -1.0, -0.5 } );
    light->innerAngle = glm::radians( 10.0f );
    light->outerAngle = glm::radians( 30.0f );
    light->range =
      CalculateLightMaximumRange( light->colour, light->attenuation );
    InitializeShadowBuffer( &light->shadowBuffer, 1024 );
  }

  scene->numFogVolumes = 1;
  for ( uint32_t i = 0; i < scene->numFogVolumes; ++i )
  {
    auto volume = scene->fogVolumes + i;
    volume->colour = glm::vec3{ 0.08 };
    volume->position = { 8, 0.25, 8 };
    volume->dimensions = glm::vec3{ 20, 0.5, 20 };
    volume->density = 0.08f;
  }

  scene->sun.colour = glm::vec3{ 0.75, 0.8, 1.0 } * 0.2f;
  scene->sun.direction = glm::normalize( glm::vec3{ 0.1, -1, -0.2 } );
  InitializeShadowBuffer( &scene->sun.shadowBuffer, 2048 );
}

internal void RenderScene( Scene *scene, const glm::mat4 &viewProjection )
{
  glUseProgram( scene->geometryPassTexture.program );
  glm::mat4 levelModelMatrix( 1.0f );
  auto levelCombinedMatrix = viewProjection * levelModelMatrix;
  glUniformMatrix4fv(
    scene->geometryPassTexture.uniformLocations[U_MODEL_MATRIX], 1, GL_FALSE,
    glm::value_ptr( levelModelMatrix ) );
  glUniformMatrix4fv(
    scene->geometryPassTexture.uniformLocations[U_COMBINED_MATRIX], 1, GL_FALSE,
    glm::value_ptr( levelCombinedMatrix ) );
  glActiveTexture( GL_TEXTURE0 );
  glBindTexture( GL_TEXTURE_2D, scene->csgTexture );
  glUniform1i( scene->geometryPassTexture.uniformLocations[U_ALBEDO_TEXTURE],
               0 );
  glActiveTexture( GL_TEXTURE1 );
  glBindTexture( GL_TEXTURE_2D, scene->normalTexture );
  glUniform1i( scene->geometryPassTexture.uniformLocations[U_NORMAL_TEXTURE],
               1 );
  glUniform3fv( scene->geometryPassTexture.uniformLocations[U_CAMERA_POSITION],
                1, glm::value_ptr( scene->camera.position ) );

  OpenGLDrawStaticMesh( scene->levelGeometryMesh );

  glm::mat4 monkeyModelMatrix =
    glm::translate( glm::mat4(), glm::vec3{8, 1, -1} );
  // monkeyModelMatrix = glm::rotate(
  // monkeyModelMatrix, glm::pi<float>() * 0.25f, glm::vec3{0, 1, 0} );
  auto combined = viewProjection * monkeyModelMatrix;
  glUseProgram( scene->geometryPassColour.program );
  glUniformMatrix4fv(
    scene->geometryPassTexture.uniformLocations[U_COMBINED_MATRIX], 1, GL_FALSE,
    glm::value_ptr( combined ) );
  glUniformMatrix4fv(
    scene->geometryPassTexture.uniformLocations[U_MODEL_MATRIX], 1, GL_FALSE,
    glm::value_ptr( monkeyModelMatrix ) );
  glm::vec3 monkeyColour{1, 0, 0};
  glUniform3fv( scene->geometryPassColour.uniformLocations[U_ALBEDO_COLOUR], 1,
                glm::value_ptr( monkeyColour ) );
  OpenGLDrawStaticMesh( scene->monkeyMesh );
}

internal void GeometryPass( Scene *scene, const glm::mat4 &viewProjection )
{
  glCullFace( GL_BACK );
  if ( scene->gbuffer.fbo )
  {
    glBindFramebuffer( GL_DRAW_FRAMEBUFFER, scene->gbuffer.fbo );
    glDepthMask( GL_TRUE );
    glEnable( GL_DEPTH_TEST );
    glDisable( GL_BLEND );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    RenderScene( scene, viewProjection );
  }
}

internal void DecalPass( Scene *scene, const glm::mat4 &viewProjection )
{
  if ( scene->gbuffer.fbo )
  {
    auto decal = &scene->decalPass;
    glFramebufferTexture2D( GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                            GL_TEXTURE_2D, 0, 0 );
    glDepthMask( GL_FALSE );
    glEnable( GL_DEPTH_TEST );
    glEnable( GL_BLEND );
    glBlendEquation( GL_FUNC_ADD );
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    glCullFace( GL_FRONT );

    glUseProgram( decal->program );
    glm::mat4 decalModelMatrix =
      glm::translate( glm::mat4(), glm::vec3{4, 1.5, -2.0} );
    decalModelMatrix = glm::scale( decalModelMatrix, glm::vec3{0.2f} );
    auto decalCombined = viewProjection * decalModelMatrix;
    glUniformMatrix4fv( decal->uniformLocations[U_COMBINED_MATRIX], 1,
                        GL_FALSE, glm::value_ptr( decalCombined ) );
    glUniformMatrix4fv( decal->uniformLocations[U_MODEL_MATRIX], 1,
                        GL_FALSE, glm::value_ptr( decalModelMatrix ) );
    glActiveTexture( GL_TEXTURE0 );
    glBindTexture( GL_TEXTURE_2D, scene->gbuffer.depthTexture );
    glUniform1i( decal->uniformLocations[U_DEPTH_TEXTURE], 0 );

    glm::vec2 screenSize = {windowWidth, windowHeight};
    glUniform2fv( decal->uniformLocations[U_SCREEN_SIZE], 1,
                  glm::value_ptr( screenSize ) );
    glm::mat4 invViewProjection = glm::inverse( viewProjection );
    glUniformMatrix4fv( decal->uniformLocations[U_INVERSE_VIEW_PROJECTION], 1,
                        GL_FALSE, glm::value_ptr( invViewProjection ) );
    glm::mat4 invModelMatrix = glm::inverse( decalModelMatrix );
    glUniformMatrix4fv( decal->uniformLocations[U_INVERSE_MODEL_MATRIX], 1,
                        GL_FALSE, glm::value_ptr( invModelMatrix ) );
    glUniform3fv( decal->uniformLocations[U_CAMERA_POSITION], 1,
                  glm::value_ptr( scene->camera.position ) );
    glActiveTexture( GL_TEXTURE1 );
    glBindTexture( GL_TEXTURE_2D, scene->bulletHoleTexture );
    glUniform1i( decal->uniformLocations[U_ALBEDO_TEXTURE], 1 );
    glActiveTexture( GL_TEXTURE2 );
    glBindTexture( GL_TEXTURE_2D, scene->bulletHoleNormalTexture );
    glUniform1i( decal->uniformLocations[U_NORMAL_TEXTURE], 2 );
    OpenGLDrawStaticMesh( scene->cubeMesh );
    glFramebufferTexture2D( GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                            GL_TEXTURE_2D, scene->gbuffer.depthTexture, 0 );
    glActiveTexture( GL_TEXTURE0 );
    glBindTexture( GL_TEXTURE_2D, 0 );
  }
}

inline glm::mat4 BuildChangeOfBasisMatrix( glm::vec3 v )
{
  v = glm::normalize( v );
  glm::mat3 result;
  if ( v.x == 0 && v.z == 0 )
  {
    if ( v.y < 0 )
    {
      result =
        glm::mat3{glm::vec3{-1, 0, 0}, glm::vec3{0, -1, 0}, glm::vec3{0, 0, 1}};
    }
  }
  else
  {
    glm::vec3 newZ = glm::cross( v, glm::vec3{ 0, 1, 0 } );
    glm::vec3 newX = glm::cross( v, newZ );
    result = glm::mat3{ newX, v, newZ };
  }
  return glm::mat4( result );
}

inline glm::mat4 BuildChangeOfBasisMatrix2( const glm::vec3 &direction,
                                            const glm::vec3 &up = glm::vec3{
                                              0, 1, 0} )
{
  glm::mat3 result;
  // TODO: Handle cases.
  glm::vec3 newX = glm::cross( direction, up );
  glm::vec3 newY = glm::cross( newX, direction );
  glm::vec3 newZ = direction;
  result = glm::mat3{ newX, newY, newZ };
  return glm::mat4( result );
}

internal void ShadowPass( Scene *scene )
{
  glDepthMask( GL_TRUE );
  glDisable( GL_BLEND );
  glEnable( GL_DEPTH_TEST );
  glCullFace( GL_FRONT );
  for ( uint32_t i = 0; i < scene->numSpotLights; ++i )
  {
    auto light = scene->spotLights + i;
    auto shadowBuffer = &light->shadowBuffer;
    if ( shadowBuffer->fbo )
    {
      glBindFramebuffer( GL_FRAMEBUFFER,  shadowBuffer->fbo );
      glViewport( 0, 0,  shadowBuffer->size, shadowBuffer->size );
      glClear( GL_DEPTH_BUFFER_BIT );

      glm::mat4 projectionMatrix =
        glm::perspective( light->outerAngle, 1.0f, 0.2f, 40.0f );

      glm::mat4 viewMatrix;
      viewMatrix *= glm::transpose(
        BuildChangeOfBasisMatrix2( -light->direction, glm::vec3{0, 1, 0} ) );
      viewMatrix = glm::translate( viewMatrix, -light->position );

      light->viewProjection = projectionMatrix * viewMatrix;

      RenderScene( scene, light->viewProjection );
    }
  }
  float scale = 16.0f;
  glm::mat4 projectionMatrix =
    glm::ortho( -scale, scale, -scale, scale, -scale, scale );

  auto sun = &scene->sun;
  glm::mat4 viewMatrix;
  viewMatrix *= glm::transpose( BuildChangeOfBasisMatrix2( -sun->direction, glm::vec3{0, 1, 0} ) );
  sun->viewProjection = projectionMatrix * viewMatrix;
  glBindFramebuffer( GL_FRAMEBUFFER,  sun->shadowBuffer.fbo );
  glViewport( 0, 0,  sun->shadowBuffer.size, sun->shadowBuffer.size );
  glClear( GL_DEPTH_BUFFER_BIT );
  RenderScene( scene, sun->viewProjection );

  glBindFramebuffer( GL_FRAMEBUFFER, 0 );
  glViewport( 0, 0, windowWidth, windowHeight );
}

internal void LightingPass( Scene *scene, const glm::mat4 viewProjection )
{
  if ( scene->hdrBuffer.fbo )
  {
    glBindFramebuffer( GL_DRAW_FRAMEBUFFER, scene->hdrBuffer.fbo );
    glDisable( GL_DEPTH_TEST );
    glDepthMask( GL_FALSE );
    glEnable( GL_BLEND );
    glClear( GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT );

    glBlendEquation( GL_FUNC_ADD );
    glBlendFunc( GL_ONE, GL_ONE );

    glm::mat4 invViewProjection = glm::inverse( viewProjection );
    glm::vec2 screenSize = {windowWidth, windowHeight};

    glActiveTexture( GL_TEXTURE0 );
    glBindTexture( GL_TEXTURE_2D, scene->gbuffer.albedoTexture );
    glActiveTexture( GL_TEXTURE1 );
    glBindTexture( GL_TEXTURE_2D, scene->gbuffer.normalTexture );
    glActiveTexture( GL_TEXTURE2 );
    glBindTexture( GL_TEXTURE_2D, scene->gbuffer.depthTexture );
    glActiveTexture( GL_TEXTURE3 );
    glBindTexture( GL_TEXTURE_2D, scene->gbuffer.surfaceTexture );

    auto point = &scene->pointLightShader;
    glUseProgram( point->program );
    glUniform1i( point->uniformLocations[U_ALBEDO_TEXTURE], 0 );
    glUniform1i( point->uniformLocations[U_NORMAL_TEXTURE], 1 );
    glUniform1i( point->uniformLocations[U_DEPTH_TEXTURE], 2 );
    glUniform1i( point->uniformLocations[U_SURFACE_TEXTURE], 3 );
    glUniformMatrix4fv( point->uniformLocations[U_INVERSE_VIEW_PROJECTION], 1,
                        GL_FALSE, glm::value_ptr( invViewProjection ) );
    glUniform2fv( point->uniformLocations[U_SCREEN_SIZE], 1,
                  glm::value_ptr( screenSize ) );
    glUniform3fv( point->uniformLocations[U_CAMERA_POSITION], 1,
                  glm::value_ptr( scene->camera.position ) );

    glCullFace( GL_FRONT );

    for ( uint32_t i = 0; i < scene->numPointLights; ++i )
    {
      auto light = scene->pointLights + i;
      glUniform3fv( point->uniformLocations[U_LIGHT_POSITION], 1,
                    glm::value_ptr( light->position ) );

      glm::mat4 modelMatrix = glm::translate( glm::mat4(), light->position );
      modelMatrix = glm::scale( modelMatrix, glm::vec3{light->radius} );
      glm::mat4 combined = viewProjection * modelMatrix;
      glUniformMatrix4fv( point->uniformLocations[U_COMBINED_MATRIX], 1,
                          GL_FALSE, glm::value_ptr( combined ) );
      glUniform3fv( point->uniformLocations[U_LIGHT_COLOUR], 1,
                    glm::value_ptr( light->colour ) );
      glUniform3fv( point->uniformLocations[U_LIGHT_ATTENUATION], 1,
                    glm::value_ptr( light->attenuation ) );

      OpenGLDrawStaticMesh( scene->icosphere );
    }

    auto spot = &scene->spotLightShader;
    glUseProgram( spot->program );
    glUniform1i( spot->uniformLocations[U_ALBEDO_TEXTURE], 0 );
    glUniform1i( spot->uniformLocations[U_NORMAL_TEXTURE], 1 );
    glUniform1i( spot->uniformLocations[U_DEPTH_TEXTURE], 2 );
    glUniform1i( spot->uniformLocations[U_SURFACE_TEXTURE], 3 );
    glUniformMatrix4fv( spot->uniformLocations[U_INVERSE_VIEW_PROJECTION], 1,
                        GL_FALSE, glm::value_ptr( invViewProjection ) );
    glUniform2fv( spot->uniformLocations[U_SCREEN_SIZE], 1,
                  glm::value_ptr( screenSize ) );
    glUniform3fv( spot->uniformLocations[U_CAMERA_POSITION], 1,
                  glm::value_ptr( scene->camera.position ) );

    for ( uint32_t i = 0; i < scene->numSpotLights; ++i )
    {
      auto light = scene->spotLights + i;
      glUniform3fv( spot->uniformLocations[U_LIGHT_POSITION], 1,
                    glm::value_ptr( light->position ) );

      glm::mat4 modelMatrix;
      modelMatrix = glm::translate( modelMatrix, light->position );
      float z = light->range;
      float x = glm::tan( light->outerAngle ) * z * 3.0f;
      modelMatrix *= BuildChangeOfBasisMatrix2(
        glm::normalize( light->direction ), glm::vec3{0, 1, 0} );
      modelMatrix = glm::scale( modelMatrix, glm::vec3{x, x, z} );

      glm::mat4 combined = viewProjection * modelMatrix;
      glUniformMatrix4fv( spot->uniformLocations[U_COMBINED_MATRIX], 1,
                          GL_FALSE, glm::value_ptr( combined ) );
      glUniform3fv( spot->uniformLocations[U_LIGHT_COLOUR], 1,
                    glm::value_ptr( light->colour ) );
      glUniform3fv( spot->uniformLocations[U_LIGHT_ATTENUATION], 1,
                    glm::value_ptr( light->attenuation ) );
      glUniform3fv( spot->uniformLocations[U_LIGHT_DIRECTION], 1,
                    glm::value_ptr( light->direction ) );

      glm::vec2 cone{glm::cos( light->innerAngle ),
                     glm::cos( light->outerAngle )};

      glUniform2fv( spot->uniformLocations[U_LIGHT_CONE], 1,
                    glm::value_ptr( cone ) );

      glActiveTexture( GL_TEXTURE4 );
      glBindTexture( GL_TEXTURE_2D, light->shadowBuffer.depthTexture );
      glUniform1i( spot->uniformLocations[U_SHADOW_MAP], 4 );

      glUniformMatrix4fv( spot->uniformLocations[U_LIGHT_VIEW_PROJECTION], 1,
                          GL_FALSE, glm::value_ptr( light->viewProjection ) );

      OpenGLDrawStaticMesh( scene->cone );
    }
    auto directional = &scene->directionalLightShader;
    auto sun = &scene->sun;
    glUseProgram( directional->program );
    glUniform1i( directional->uniformLocations[U_ALBEDO_TEXTURE], 0 );
    glUniform1i( directional->uniformLocations[U_NORMAL_TEXTURE], 1 );
    glUniform1i( directional->uniformLocations[U_DEPTH_TEXTURE], 2 );
    glUniform1i( directional->uniformLocations[U_SURFACE_TEXTURE], 3 );
    glActiveTexture( GL_TEXTURE4 );
    glBindTexture( GL_TEXTURE_2D, sun->shadowBuffer.depthTexture );
    glUniform1i( directional->uniformLocations[U_SHADOW_MAP], 4 );
    glUniformMatrix4fv(
      directional->uniformLocations[U_INVERSE_VIEW_PROJECTION], 1, GL_FALSE,
      glm::value_ptr( invViewProjection ) );
    glUniform3fv( directional->uniformLocations[U_CAMERA_POSITION], 1,
                  glm::value_ptr( scene->camera.position ) );
    glUniform3fv( directional->uniformLocations[U_LIGHT_COLOUR], 1,
                  glm::value_ptr( sun->colour ) );
    glUniform3fv( directional->uniformLocations[U_LIGHT_DIRECTION], 1,
                  glm::value_ptr( sun->direction ) );
    glUniformMatrix4fv( directional->uniformLocations[U_LIGHT_VIEW_PROJECTION],
                        1, GL_FALSE, glm::value_ptr( sun->viewProjection ) );
    glCullFace( GL_BACK );
    OpenGLDrawStaticMesh( scene->fullscreenQuad );
  }
}

void VolumePrePass( Scene *scene, const glm::mat4 &viewProjection )
{
  if ( scene->volumeBuffer.fbo )
  {
    glBindFramebuffer( GL_DRAW_FRAMEBUFFER, scene->volumeBuffer.fbo );
    glClear( GL_DEPTH_BUFFER_BIT );
    auto depthPass = &scene->fogDepthPass;
    glUseProgram( depthPass->program );
    glCullFace( GL_BACK );

    for ( uint32_t i = 0; i < scene->numFogVolumes; ++i )
    {
      auto volume = scene->fogVolumes + i;
      glm::mat4 modelMatrix;
      modelMatrix = glm::translate( modelMatrix, volume->position );
      modelMatrix = glm::scale( modelMatrix, volume->dimensions );
      glm::mat4 combined = viewProjection * modelMatrix;
      glUniformMatrix4fv( depthPass->uniformLocations[U_COMBINED_MATRIX], 1,
                          GL_FALSE, glm::value_ptr( combined ) );
      OpenGLDrawStaticMesh( scene->cubeMesh );
    }
    glBindFramebuffer( GL_DRAW_FRAMEBUFFER, 0 );
  }
}

void VolumePass( Scene *scene, const glm::mat4 &viewProjection )
{
  if ( scene->volumeBuffer.fbo )
  {
    glBlendEquation( GL_FUNC_ADD );
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

    glCullFace( GL_FRONT );
    auto fog = &scene->fogShader;
    glUseProgram( fog->program );
    glActiveTexture( GL_TEXTURE0 );
    glBindTexture( GL_TEXTURE_2D, scene->volumeBuffer.depthTexture );
    glUniform1i( fog->uniformLocations[U_DEPTH_TEXTURE], 0 );
    glActiveTexture( GL_TEXTURE1 );
    glBindTexture( GL_TEXTURE_2D, scene->gbuffer.depthTexture );
    glUniform1i( fog->uniformLocations[U_SCENE_DEPTH_TEXTURE], 1 );
    glActiveTexture( GL_TEXTURE2 );
    glBindTexture( GL_TEXTURE_2D, scene->fogTexture );
    glUniform1i( fog->uniformLocations[U_COLOUR_TEXTURE], 2 );

    glm::vec2 screenSize = {windowWidth, windowHeight};
    glUniform2fv( fog->uniformLocations[U_SCREEN_SIZE], 1,
                  glm::value_ptr( screenSize ) );
    glUniform3fv( fog->uniformLocations[U_CAMERA_POSITION], 1,
                  glm::value_ptr( scene->camera.position ) );
    glm::mat4 invViewProjection = glm::inverse( viewProjection );
    glUniformMatrix4fv( fog->uniformLocations[U_INVERSE_VIEW_PROJECTION], 1,
                        GL_FALSE, glm::value_ptr( invViewProjection ) );
    glUniform1f( fog->uniformLocations[U_TIME], scene->currentTime );
    for ( uint32_t i = 0; i < scene->numFogVolumes; ++i )
    {
      auto volume = scene->fogVolumes + i;
      glUniform3fv( fog->uniformLocations[U_VOLUME_COLOUR], 1,
                    glm::value_ptr( volume->colour ) );
      glUniform1f( fog->uniformLocations[U_VOLUME_DENSITY], volume->density );
      glm::mat4 modelMatrix;
      modelMatrix = glm::translate( modelMatrix, volume->position );
      modelMatrix = glm::scale( modelMatrix, volume->dimensions );
      glm::mat4 combined = viewProjection * modelMatrix;
      glUniformMatrix4fv( fog->uniformLocations[U_COMBINED_MATRIX], 1, GL_FALSE,
                          glm::value_ptr( combined ) );
      glUniformMatrix4fv( fog->uniformLocations[U_MODEL_MATRIX], 1, GL_FALSE,
                          glm::value_ptr( modelMatrix ) );
      OpenGLDrawStaticMesh( scene->cubeMesh );
    }
  }
}

internal void Render( Scene *scene, float interp )
{
  glViewport( 0, 0, windowWidth, windowHeight );
  glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
  glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
  glEnable( GL_DEPTH_TEST );
  glEnable( GL_TEXTURE_2D );

  glActiveTexture( GL_TEXTURE0 );
  glBindTexture( GL_TEXTURE_2D, 0 );
  glActiveTexture( GL_TEXTURE1 );
  glBindTexture( GL_TEXTURE_2D, 0 );
  glActiveTexture( GL_TEXTURE2 );
  glBindTexture( GL_TEXTURE_2D, 0 );

  glEnable( GL_CULL_FACE );
  glCullFace( GL_BACK );
  auto viewMatrix = CreateViewMatrix( &scene->camera, interp );
  auto viewProjection = projectionMatrix * viewMatrix;
  //viewProjection = scene->spotLights[1].viewProjection;
  if ( scene->gbuffer.fbo )
  {
    ShadowPass( scene );
    GeometryPass( scene, viewProjection );
    DecalPass( scene, viewProjection );
    //VolumePrePass( scene, viewProjection );
    LightingPass( scene, viewProjection );
    //VolumePass( scene, viewProjection );
  }

  glCullFace( GL_BACK );
  glBindFramebuffer( GL_DRAW_FRAMEBUFFER, 0 );
  glDisable( GL_DEPTH_TEST );
  glDepthMask( GL_FALSE );
  glDisable( GL_BLEND );
  glClear( GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT );

  glUseProgram( scene->postProcessingShader.program );
  glActiveTexture( GL_TEXTURE0 );
  glBindTexture( GL_TEXTURE_2D, scene->hdrBuffer.hdrTexture );
  glGenerateMipmap( GL_TEXTURE_2D );
  glActiveTexture( GL_TEXTURE1 );
  glBindTexture( GL_TEXTURE_2D, scene->gbuffer.depthTexture );
  glUniform1i( scene->postProcessingShader.uniformLocations[U_HDR_TEXTURE], 0 );
  glUniform1i( scene->postProcessingShader.uniformLocations[U_DEPTH_TEXTURE],
               1 );
  glm::vec2 screenSize{ windowWidth, windowHeight };
  glUniform2fv( scene->postProcessingShader.uniformLocations[U_SCREEN_SIZE], 1,
                glm::value_ptr( screenSize ) );

  OpenGLDrawStaticMesh( scene->fullscreenQuad );
}

int main( int argc, char **argv )
{
  if ( !glfwInit() )
  {
    fprintf( stderr, "Failed to initialize GLFW.\n" );
    return -1;
  }
  //glfwWindowHint( GLFW_DOUBLEBUFFER, 1 );
  glfwWindowHint( GLFW_SAMPLES, 0 );
  glfwWindowHint( GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE );
  GLFWwindow *window =
    glfwCreateWindow( 1280, 720, "Deferred Renderer", NULL, NULL );
  windowWidth = 1280;
  windowHeight = 720;
  if ( !window )
  {
    glfwTerminate();
    fprintf( stderr, "Failed to create GLFW window.\n" );
    return -1;
  }
  WindowResizeCallback( window, windowWidth, windowHeight );

  glfwMakeContextCurrent( window );
  glfwSetWindowSizeCallback( window, WindowResizeCallback );
  glfwSetKeyCallback( window, KeyCallback );
  glfwSetMouseButtonCallback( window, MouseButtonCallback );
  glfwSetScrollCallback( window, uiOnScroll );
  glfwSetCharCallback( window, uiOnCharInput );
  glfwSwapInterval( 1 );

  GLenum err = glewInit();
  if ( err != GLEW_OK )
  {
    fprintf( stderr, "GLEW ERROR: %s\n", glewGetErrorString( err ) );
    return -1;
  }
  fprintf( stdout, "Using GLEW %s.\n", glewGetString( GLEW_VERSION ) );

  glEnable( GL_DEBUG_OUTPUT_SYNCHRONOUS );
  glDebugMessageCallback( OpenGLReportErrorMessage, nullptr );
  GLuint unusedIds = 0;
  glDebugMessageControl( GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0,
                         &unusedIds, GL_TRUE );

  Scene scene = {};
  LoadContent( &scene );
  uiInit( window );

  double t = 0.0;
  float logicHz = 30.0f;
  float logicTimestep = 1.0f / logicHz;
  double maxFrameTime = 0.25; // Used to prevent the simulation from being
                              // affected by break points.

  double currentTime = glfwGetTime();
  double accumulator = 0.0;

  double prevMouseX = 0.0, prevMouseY = 0.0;
  scene.camera.position = glm::vec3{ 0, 1, 4 };
  while ( run )
  {
    double newTime = glfwGetTime();
    double frameTime = newTime - currentTime;
    if ( frameTime > maxFrameTime )
      frameTime = maxFrameTime;
    currentTime = newTime;

    accumulator += frameTime;

    while ( accumulator >= logicTimestep )
    {
      glfwPollEvents();
      if ( glfwWindowShouldClose( window ) )
      {
        run = false;
      }
      if ( windowWasResized )
      {
        windowWasResized = false;
        DeinitializeGBuffer( &scene.gbuffer );
        InitializeGBuffer( &scene.gbuffer, windowWidth, windowHeight );
        DeinitializeHDRBuffer( &scene.hdrBuffer );
        InitializeHDRBuffer( &scene.hdrBuffer, windowWidth, windowHeight );
        DeinitializeVolumetricBuffer( &scene.volumeBuffer );
        InitializeVolumetricBuffer( &scene.volumeBuffer, windowWidth,
                                    windowHeight );
      }

      glm::vec2 rotation;
      glm::vec3 velocity;

      double mousePosX, mousePosY;
      glfwGetCursorPos( window, &mousePosX, &mousePosY );
      double dx = mousePosX - prevMouseX;
      double dy = mousePosY - prevMouseY;
      prevMouseX = mousePosX;
      prevMouseY = mousePosY;

      if ( enableMouseLook )
      {
        rotation.x = dy;
        rotation.y = dx;
        rotation *= 0.03f;

        float cameraSpeed = 2.0f;
        if ( keyStates & KEY_W )
        {
          velocity.z = -cameraSpeed;
        }
        if ( keyStates & KEY_S )
        {
          velocity.z = cameraSpeed;
        }
        if ( keyStates & KEY_A )
        {
          velocity.x = -cameraSpeed;
        }
        if ( keyStates & KEY_D )
        {
          velocity.x = cameraSpeed;
        }
      }
      UpdateCamera( &scene.camera, rotation, velocity, logicTimestep );
      t += logicTimestep;
      accumulator -= logicTimestep;
      scene.currentTime += logicTimestep;
    }

    double interp = accumulator / logicTimestep;
    Render( &scene, interp );
    double mousePosX, mousePosY;
    glfwGetCursorPos( window, &mousePosX, &mousePosY );
    uiNewFrame( mousePosX, mousePosY, currentTime, logicTimestep );
    ImGui::Render();
    glfwSwapBuffers( window );
  }
  glfwTerminate();
  return 0;
}
