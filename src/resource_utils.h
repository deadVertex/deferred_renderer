#pragma once

#include <cstdint>

#include "opengl_utils.h"
#include "utils.h"

extern int LoadEffect( const char *path );

extern int CompileProgram( int effect, const char *programName );

extern void DeleteEffect( int effect );

enum
{
  TEXTURE_MIPMAPS = BIT( 0 ),
  TEXTURE_NORMAL_MAP = BIT( 1 ),
};

extern uint32_t LoadTexture( const char *path,
                             uint8_t flags = 0,
                             uint8_t format = PIXEL_FORMAT_RGB8 );

extern OpenGLStaticMesh LoadMesh( const char *path );
