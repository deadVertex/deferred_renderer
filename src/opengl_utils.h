#pragma once

#include <glm/vec3.hpp>
#include <glm/vec2.hpp>

#define GLEW_STATIC
#include <GL/glew.h>

#define INVALID_OPENGL_SHADER 0
#define INVALID_OPENGL_TEXTURE 0

enum
{
  VERTEX_ATTRIBUTE_POSITION = 0,
  VERTEX_ATTRIBUTE_NORMAL = 1,
  VERTEX_ATTRIBUTE_TEXTURE_COORDINATE = 2,
  VERTEX_ATTRIBUTE_COLOUR = 3,
  VERTEX_ATTRIBUTE_TANGENT = 4,
  VERTEX_ATTRIBUTE_BITANGENT = 5,
  MAX_VERTEX_ATTRIBUTES,
};

// TODO: Figure out a good place for these.
enum
{
  VERTEX_DATA_POSITION = 1,
  VERTEX_DATA_NORMAL = 2,
  VERTEX_DATA_TEXTURE_COORDINATE = 4,
};

enum
{
  PIXEL_FORMAT_RGB8,
  PIXEL_FORMAT_RGBA8,
  PIXEL_FORMAT_NORMAL_MAP, // TODO: Use a different system for this information.
};

enum
{
  PIXEL_COMPONENT_TYPE_UINT8
};

struct OpenGLStaticMesh
{
  uint32_t vao, vbo, ibo, numIndices, numVertices, primitive, indexType;
};

typedef void ( *OpenGLErrorMessageCallback )( const char * );

extern void OpenGLDeleteShader( uint32_t program );

extern uint32_t OpenGLCreateShader( const char *vertSource, uint32_t vertLength,
                                    const char *fragSource, uint32_t fragLength,
                                    OpenGLErrorMessageCallback callback );

struct OpenGLVertexAttribute
{
  GLuint index;
  GLint numComponents;
  GLenum componentType;
  GLboolean normalized;
  GLsizei offset;
};

extern void OpenGLDeleteStaticMesh( OpenGLStaticMesh mesh );

extern OpenGLStaticMesh
OpenGLCreateStaticMesh( const void *vertices, uint32_t numVertices,
                        uint32_t *indices, uint32_t numIndices,
                        uint32_t vertexSize,
                        OpenGLVertexAttribute *vertexAttributes,
                        uint32_t numVertexAttributes, GLenum primitive );
extern void OpenGLDrawStaticMesh( OpenGLStaticMesh mesh );

extern OpenGLStaticMesh CreateWireframeCube();
extern OpenGLStaticMesh CreateAxisMesh();
extern OpenGLStaticMesh CreateFullscreenQuadMesh();
extern OpenGLStaticMesh CreatePlaneMesh( float width = 1.0f,
                                         float height = 1.0f,
                                         float textureScale = 1.0f );
extern void OpenGLDeleteTexture( uint32_t texture );
extern uint32_t OpenGLCreateTexture( uint32_t width, uint32_t height,
                                     int pixelFormat, int pixelComponentType,
                                     const void *pixels );

extern OpenGLStaticMesh CreateCubeMesh();
extern OpenGLStaticMesh CreateIcosahedronMesh( uint32_t tesselationLevel = 0 );
extern OpenGLStaticMesh CreateCone( uint32_t segments );

struct TangentData
{
  glm::vec3 tangent, bitangent;
};

extern TangentData ComputeTangentData( const glm::vec3 &a, const glm::vec2 &h,
                                       const glm::vec3 &b, const glm::vec2 &i,
                                       const glm::vec3 &c, const glm::vec2 &j,
                                       const glm::vec3 &N );
