
#include "render.h"

#include "opengl_utils.h"
#include "utils.h"

#include <cstdio>

bool InitializeGBuffer( GBuffer *gbuffer, uint32_t width, uint32_t height )
{
  gbuffer->width = width;
  gbuffer->height = height;

  glGenFramebuffers( 1, &gbuffer->fbo );
  glBindFramebuffer( GL_DRAW_FRAMEBUFFER, gbuffer->fbo );

  glGenTextures( 1, &gbuffer->albedoTexture );
  glBindTexture( GL_TEXTURE_2D, gbuffer->albedoTexture );
  glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB8, gbuffer->width, gbuffer->height, 0,
                GL_RGB, GL_UNSIGNED_BYTE, NULL );
  glFramebufferTexture2D( GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                          GL_TEXTURE_2D, gbuffer->albedoTexture, 0 );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );

  glGenTextures( 1, &gbuffer->normalTexture );
  glBindTexture( GL_TEXTURE_2D, gbuffer->normalTexture );
  glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB32F, gbuffer->width, gbuffer->height, 0,
                GL_RGB, GL_FLOAT, NULL );
  glFramebufferTexture2D( GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT1,
                          GL_TEXTURE_2D, gbuffer->normalTexture, 0 );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );

  glGenTextures( 1, &gbuffer->surfaceTexture );
  glBindTexture( GL_TEXTURE_2D, gbuffer->surfaceTexture );
  glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB8, gbuffer->width, gbuffer->height, 0,
                GL_RGB, GL_UNSIGNED_BYTE, NULL );
  glFramebufferTexture2D( GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT2,
                          GL_TEXTURE_2D, gbuffer->surfaceTexture, 0 );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );

  glGenTextures( 1, &gbuffer->depthTexture );
  glBindTexture( GL_TEXTURE_2D, gbuffer->depthTexture );
  glTexImage2D( GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, gbuffer->width,
                gbuffer->height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL );
  glFramebufferTexture2D( GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                          GL_TEXTURE_2D, gbuffer->depthTexture, 0 );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );


  uint32_t drawBuffers[3];
  drawBuffers[0] = GL_COLOR_ATTACHMENT0;
  drawBuffers[1] = GL_COLOR_ATTACHMENT1;
  drawBuffers[2] = GL_COLOR_ATTACHMENT2;
  glDrawBuffers( ARRAY_COUNT( drawBuffers ), drawBuffers );

  GLenum status = glCheckFramebufferStatus( GL_DRAW_FRAMEBUFFER );
  if ( status != GL_FRAMEBUFFER_COMPLETE )
  {
    printf( "Framebuffer error, status: 0x%x\n", status );
    return false;
  }
  glBindFramebuffer( GL_DRAW_FRAMEBUFFER, 0 );
  return true;
}

void DeinitializeGBuffer( GBuffer *gbuffer )
{
	if ( gbuffer->fbo )
	{
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

		glDeleteTextures(1, &gbuffer->albedoTexture);
		glDeleteTextures(1, &gbuffer->depthTexture);
		glDeleteTextures(1, &gbuffer->normalTexture);
    glDeleteTextures(1, &gbuffer->surfaceTexture );

		glDeleteFramebuffers(1, &gbuffer->fbo);
		gbuffer->fbo = 0;
	}
}

bool InitializeHDRBuffer( HDRBuffer *buffer, uint32_t width, uint32_t height )
{
  buffer->width = width;
  buffer->height = height;

  glGenFramebuffers( 1, &buffer->fbo );
  glBindFramebuffer( GL_DRAW_FRAMEBUFFER, buffer->fbo );

  glGenTextures( 1, &buffer->hdrTexture );
  glBindTexture( GL_TEXTURE_2D, buffer->hdrTexture );
  glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB16F, buffer->width, buffer->height, 0,
                GL_RGB, GL_UNSIGNED_BYTE, NULL );
  glFramebufferTexture2D( GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                          GL_TEXTURE_2D, buffer->hdrTexture, 0 );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                   GL_LINEAR_MIPMAP_LINEAR );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                   GL_LINEAR_MIPMAP_LINEAR );

  glGenerateMipmap( GL_TEXTURE_2D );
  glDrawBuffer( GL_COLOR_ATTACHMENT0 );

  GLenum status = glCheckFramebufferStatus( GL_DRAW_FRAMEBUFFER );
  if ( status != GL_FRAMEBUFFER_COMPLETE )
  {
    printf( "Framebuffer error, status: 0x%x\n", status );
    return false;
  }
  glBindFramebuffer( GL_DRAW_FRAMEBUFFER, 0 );
  return true;
}

void DeinitializeHDRBuffer( HDRBuffer *buffer )
{
	if ( buffer->fbo )
	{
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

		glDeleteTextures(1, &buffer->hdrTexture);

		glDeleteFramebuffers(1, &buffer->fbo);
		buffer->fbo = 0;
	}
}

bool InitializeVolumetricBuffer( VolumetricBuffer *buffer, uint32_t width,
                                 uint32_t height )
{
  buffer->width = width;
  buffer->height = height;

  glGenFramebuffers( 1, &buffer->fbo );
  glBindFramebuffer( GL_DRAW_FRAMEBUFFER, buffer->fbo );

  glGenTextures( 1, &buffer->depthTexture );
  glBindTexture( GL_TEXTURE_2D, buffer->depthTexture );
  glTexImage2D( GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, buffer->width,
                buffer->height, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL );
  glFramebufferTexture2D( GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                          GL_TEXTURE_2D, buffer->depthTexture, 0 );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );

  glDrawBuffer( GL_COLOR_ATTACHMENT0 );

  GLenum status = glCheckFramebufferStatus( GL_DRAW_FRAMEBUFFER );
  if ( status != GL_FRAMEBUFFER_COMPLETE )
  {
    printf( "Framebuffer error, status: 0x%x\n", status );
    return false;
  }
  glBindFramebuffer( GL_DRAW_FRAMEBUFFER, 0 );
  return true;
}

void DeinitializeVolumetricBuffer( VolumetricBuffer *buffer )
{
  if ( buffer->fbo )
  {
    glBindFramebuffer( GL_DRAW_BUFFER, 0 );
    glDeleteTextures( 1, &buffer->depthTexture );
    glDeleteFramebuffers( 1, &buffer->fbo );
    buffer->fbo = 0;
  }
}

bool InitializeShadowBuffer( ShadowBuffer *buffer, uint32_t size )
{
  buffer->size = size;
  glGenFramebuffers( 1, &buffer->fbo );
  glBindFramebuffer( GL_DRAW_FRAMEBUFFER, buffer->fbo );

  // TODO: Change GL_FLOAT to GL_UNSIGNED_BYTE.
  glGenTextures( 1, &buffer->depthTexture );
  glBindTexture( GL_TEXTURE_2D, buffer->depthTexture );
  glTexImage2D( GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, buffer->size,
                buffer->size, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE,
                   GL_COMPARE_REF_TO_TEXTURE );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL );
  glFramebufferTexture2D( GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                          GL_TEXTURE_2D, buffer->depthTexture, 0 );

  glDrawBuffer( GL_COLOR_ATTACHMENT0 );

  GLenum status = glCheckFramebufferStatus( GL_DRAW_FRAMEBUFFER );
  if ( status != GL_FRAMEBUFFER_COMPLETE )
  {
    printf( "Framebuffer error, status: 0x%x\n", status );
    return false;
  }
  glBindFramebuffer( GL_DRAW_FRAMEBUFFER, 0 );
  return true;
}

bool InitializeRenderPassBuffer( RenderPassBuffer *buffer, uint32_t size )
{
  buffer->size = size;

  glGenFramebuffers( 1, &buffer->fbo );
  glBindFramebuffer( GL_DRAW_FRAMEBUFFER, buffer->fbo );

  glGenTextures( 1, &buffer->texture );
  glBindTexture( GL_TEXTURE_2D, buffer->texture );
  glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB8, buffer->size, buffer->size, 0,
                GL_RGB, GL_UNSIGNED_BYTE, NULL );
  glFramebufferTexture2D( GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                          GL_TEXTURE_2D, buffer->texture, 0 );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

  glDrawBuffer( GL_COLOR_ATTACHMENT0 );

  GLenum status = glCheckFramebufferStatus( GL_DRAW_FRAMEBUFFER );
  if ( status != GL_FRAMEBUFFER_COMPLETE )
  {
    printf( "Framebuffer error, status: 0x%x\n", status );
    return false;
  }
  glBindFramebuffer( GL_DRAW_FRAMEBUFFER, 0 );
  return true;
}

void DeinitializeRenderPassBuffer( RenderPassBuffer *buffer, uint32_t size )
{
  if ( buffer->fbo )
  {
    glBindFramebuffer( GL_DRAW_BUFFER, 0 );
    glDeleteTextures( 1, &buffer->texture );
    glDeleteFramebuffers( 1, &buffer->fbo );
    buffer->fbo = 0;
  }
}
