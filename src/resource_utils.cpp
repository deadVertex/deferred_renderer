#include "resource_utils.h"

#include "utils.h"
#include "opengl_utils.h"

#include <glm/glm.hpp>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#define STB_IMAGE_IMPLEMENTATION
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wparentheses"
#pragma GCC diagnostic ignored "-Wsign-compare"
#include <stb_image.h>
#pragma GCC diagnostic pop

#include <glfx.h>

struct ReadFileResult
{
  void *memory;
  uint32_t size;
};

internal uint32_t GetFileSize( FILE *file )
{
  fseek( file, 0, SEEK_END );
  uint32_t result = ftell( file );
  rewind( file );
  return result;
}

internal ReadFileResult ReadEntireFile( const char *path )
{
  ReadFileResult result = {};
  FILE *file = fopen( path, "rb" );
  if ( file )
  {
    result.size = GetFileSize( file );
    // Allocate an extra byte which can be used to store a nul terminator.
    result.memory = malloc( result.size + 1 );
    if ( result.memory )
    {
      if ( fread( result.memory, 1, result.size, file ) != result.size )
      {
        free( result.memory );
        result.memory = nullptr;
        result.size = 0;
      }
    }
    else
    {
      result.size = 0;
    }
    fclose( file );
  }
  return result;
}

internal void FreeFileMemory( void *data )
{
  free( data );
}

uint32_t LoadTexture( const char *path, uint8_t flags, uint8_t format )
{
  uint32_t result = 0;
  ReadFileResult imageFileData = ReadEntireFile( path );
  int expectedComponents = 3;
  if ( format == PIXEL_FORMAT_RGBA8 )
  {
    expectedComponents = 4;
  }
  int w, h, n;
  auto data =
    stbi_load_from_memory( (const uint8_t *)imageFileData.memory,
                           imageFileData.size, &w, &h, &n, expectedComponents );
  FreeFileMemory( imageFileData.memory );
  if ( !data )
  {
    printf( "Failed to import image: %s\n", path );
    return result;
  }

  uint32_t pitch = w * n;
  uint8_t *tempRow = new uint8_t[ pitch ];
  for ( int i = 0; i < h / 2; ++i )
  {
    auto row1 = data + ( i * pitch );
    auto row2 = data + ( ( h - i - 1 ) * pitch );
    memcpy( tempRow, row1, pitch );
    memcpy( row1, row2, pitch );
    memcpy( row2, tempRow, pitch );
  }
  delete[] tempRow;

  if ( n == 4 )
  {
    result = OpenGLCreateTexture( w, h, PIXEL_FORMAT_RGBA8,
                                  PIXEL_COMPONENT_TYPE_UINT8, data );
  }
  else
  {
    if ( flags & TEXTURE_NORMAL_MAP )
    {
      result = OpenGLCreateTexture( w, h, PIXEL_FORMAT_NORMAL_MAP,
                                    PIXEL_COMPONENT_TYPE_UINT8, data );
    }
    else
    {
      result = OpenGLCreateTexture( w, h, PIXEL_FORMAT_RGB8,
                                    PIXEL_COMPONENT_TYPE_UINT8, data );
    }
  }
  if ( flags & TEXTURE_MIPMAPS )
  {
    glBindTexture( GL_TEXTURE_2D, result );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                     GL_LINEAR_MIPMAP_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                     GL_LINEAR_MIPMAP_LINEAR );
    glGenerateMipmap( GL_TEXTURE_2D );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.0f );
  }
  stbi_image_free( data );
  return result;
}

int LoadEffect( const char *path )
{
  int result = -1;
  ReadFileResult fileData = ReadEntireFile( path );
  if ( fileData.memory )
  {
    result = glfxGenEffect();
    char *data = (char*)fileData.memory;
    data[fileData.size] = '\0';
    if ( !glfxParseEffectFromMemory( result, (char*)fileData.memory ) )
    {
      char log[10000];
      glfxGetEffectLog( result, log, sizeof( log ) );
      printf( "Error parsing effect: %s\n", log );
      glfxDeleteEffect( result );
      result = -1;
    }
    FreeFileMemory( fileData.memory );
  }
  return result;
}

int CompileProgram( int effect, const char *programName )
{
  int result = glfxCompileProgram( effect, programName );
  if ( result < 0 )
  {
    char log[10000];
    glfxGetEffectLog( effect, log, sizeof( log ) );
    printf( "Error parsing effect: %s\n", log );
  }
  return result;
}

void DeleteEffect( int effect )
{
  glfxDeleteEffect( effect );
}

struct Vertex
{
  glm::vec3 position, normal, tangent, bitangent;
  glm::vec2 texCoord;
};

internal bool BuildMesh( const aiScene *scene, OpenGLStaticMesh *result,
                         const aiNode *node = nullptr )
{
  if ( !node )
  {
    node = scene->mRootNode;
  }
  if ( node->mNumMeshes != 0 )
  {
    auto mesh = scene->mMeshes[0];
    if ( !mesh->HasPositions() ||
         !mesh->HasNormals() || !mesh->HasTextureCoords(0) ||
         !mesh->HasTangentsAndBitangents() )
    {
      printf( "Mesh does not have the required data.\n" );
      return result;
    }

    std::vector<Vertex> vertices;
    for ( uint32_t i = 0; i < mesh->mNumVertices; ++i )
    {
      auto p = mesh->mVertices + i;
      auto n = mesh->mNormals + i;
      auto t = mesh->mTextureCoords[0] + i;
      auto tn = mesh->mTangents + i;
      auto b = mesh->mBitangents + i;
      Vertex v;
      v.position = glm::vec3{p->x, p->y, p->z};
      v.normal = glm::vec3{n->x, n->y, n->z};
      v.texCoord = glm::vec2{t->x, t->y};
      v.tangent = glm::vec3{tn->x, tn->y, tn->z};
      v.bitangent = glm::vec3{b->x, b->y, b->z};
      vertices.push_back( v );
    }

    std::vector<uint32_t> indices;
    indices.resize( mesh->mNumFaces * 3 );
    uint32_t idx = 0;
    for ( uint32_t i = 0; i < mesh->mNumFaces; ++i )
    {
      auto face = mesh->mFaces + i;
      indices[idx++] = face->mIndices[0];
      indices[idx++] = face->mIndices[1];
      indices[idx++] = face->mIndices[2];
    }
    OpenGLVertexAttribute attribs[5];
    attribs[0].index = VERTEX_ATTRIBUTE_POSITION;
    attribs[0].numComponents = 3;
    attribs[0].componentType = GL_FLOAT;
    attribs[0].normalized = GL_FALSE;
    attribs[0].offset = 0;
    attribs[1].index = VERTEX_ATTRIBUTE_NORMAL;
    attribs[1].numComponents = 3;
    attribs[1].componentType = GL_FLOAT;
    attribs[1].normalized = GL_FALSE;
    attribs[1].offset = offsetof( Vertex, normal );
    attribs[2].index = VERTEX_ATTRIBUTE_TEXTURE_COORDINATE;
    attribs[2].numComponents = 2;
    attribs[2].componentType = GL_FLOAT;
    attribs[2].normalized = GL_FALSE;
    attribs[2].offset = offsetof( Vertex, texCoord );
    attribs[3].index = VERTEX_ATTRIBUTE_TANGENT;
    attribs[3].numComponents = 3;
    attribs[3].componentType = GL_FLOAT;
    attribs[3].normalized = GL_FALSE;
    attribs[3].offset = offsetof( Vertex, tangent );
    attribs[4].index = VERTEX_ATTRIBUTE_BITANGENT;
    attribs[4].numComponents = 3;
    attribs[4].componentType = GL_FLOAT;
    attribs[4].normalized = GL_FALSE;
    attribs[4].offset = offsetof( Vertex, bitangent );
    *result = OpenGLCreateStaticMesh(
      vertices.data(), vertices.size(), indices.data(), indices.size(),
      sizeof( Vertex ), attribs, 5, GL_TRIANGLES );
    return true;
  }
  for ( uint32_t i = 0; i < node->mNumChildren; ++i )
  {
    if ( BuildMesh( scene, result, node->mChildren[i] ) )
    {
      return true;
    }
  }
  return false;
}

OpenGLStaticMesh LoadMesh( const char *path )
{
  OpenGLStaticMesh result = {};
  Assimp::Importer importer;
  auto scene = importer.ReadFile(
    path, aiProcess_CalcTangentSpace | aiProcess_Triangulate |
            aiProcess_JoinIdenticalVertices | aiProcess_GenSmoothNormals |
            aiProcess_SortByPType );

  if ( !scene )
  {
    printf( "Error importing mesh: %s\n", importer.GetErrorString() );
    return result;
  }

  if ( !BuildMesh( scene, &result ) )
  {
    printf( "No mesh data found!\n" );
  }

  return result;
}
