#include "opengl_utils.h"
#include "utils.h"

#include <cstring>
#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

internal bool CompileShader( GLuint shader, const char* src, int len,
    bool isVertex, OpenGLErrorMessageCallback callback )
{
  glShaderSource( shader, 1, &src, &len );
  glCompileShader( shader );

  int success;
  glGetShaderiv( shader, GL_COMPILE_STATUS, &success );
  if ( !success ) // Check if the shader was compiled successfully.
  {
    // Prepend the shader type to the shader error log.
    char buffer[ 2048 ];
    char *cursor = buffer;
    int len = 0;
    if ( isVertex )
    {
      strcpy( buffer, "[VERTEX]\n" );
      cursor += strlen( buffer );
    }
    else
    {
      strcpy( buffer, "[FRAGMENT]\n" );
      cursor += strlen( buffer );
    }
    glGetShaderInfoLog( shader, 2048 - ( cursor - buffer ), &len, cursor );
    callback( buffer );
    return false;
  }
  return true;
}

internal bool LinkShader( GLuint vertex, GLuint fragment, GLuint program,
    OpenGLErrorMessageCallback callback )
{
  glAttachShader( program, vertex );
  glAttachShader( program, fragment );
  glLinkProgram( program);

  int success;
  glGetProgramiv( program, GL_LINK_STATUS, &success );
  if ( !success ) // Check if the shader was linked successfully.
  {
    // Prepend the string [LINKER] to the shader link error log.
    char buffer[ 2048 ];
    int len = 0;
    char *cursor = buffer;
    strcpy( buffer, "[LINKER]\n");
    cursor += strlen( buffer );
    glGetProgramInfoLog( program, 2048 - ( cursor - buffer ), &len,
        cursor );
    callback( buffer );
    return false;
  }
  return true;
}

void OpenGLDeleteShader( uint32_t program )
{
  glUseProgram( 0 );
  GLsizei count = 0;
  GLuint shaders[ 2 ];
  // Need to retrieve the vertex and fragment shaders and delete them
  // explicitly, deleting the shader program only detaches the two shaders, it
  // does not free the resources for them.
  glGetAttachedShaders( program, 2, &count, shaders );
  glDeleteProgram( program );
  glDeleteShader( shaders[ 0 ] );
  glDeleteShader( shaders[ 1 ] );
}

uint32_t OpenGLCreateShader( const char *vertSource, uint32_t vertLength,
                             const char *fragSource, uint32_t fragLength,
                             OpenGLErrorMessageCallback callback )
{
  GLuint vertex = glCreateShader( GL_VERTEX_SHADER );
  GLuint fragment = glCreateShader( GL_FRAGMENT_SHADER );
  GLuint program = glCreateProgram();

  bool vertexSuccess = CompileShader( vertex, vertSource, vertLength, true,
      callback );
  bool fragmentSuccess = CompileShader( fragment, fragSource, fragLength,
      false, callback );

  if ( vertexSuccess && fragmentSuccess )
  {
    if ( LinkShader( vertex, fragment, program, callback ) )
    {
      return program;
    }
  }
  OpenGLDeleteShader( program );
  return INVALID_OPENGL_SHADER;
}

void OpenGLDeleteStaticMesh( OpenGLStaticMesh mesh )
{
  glBindVertexArray( 0 );
  glDeleteVertexArrays( 1, &mesh.vao );
  glDeleteBuffers( 1, &mesh.vbo );
  if ( mesh.numIndices )
  {
    glDeleteBuffers( 1, &mesh.ibo );
  }
}

inline const void* ConvertUint32ToPointer( uint32_t i )
{
  return (const void*)( (uint8_t*)0 + i );
}

OpenGLStaticMesh
OpenGLCreateStaticMesh( const void *vertices, uint32_t numVertices,
                        uint32_t *indices, uint32_t numIndices,
                        uint32_t vertexSize,
                        OpenGLVertexAttribute *vertexAttributes,
                        uint32_t numVertexAttributes, GLenum primitive )
{
  OpenGLStaticMesh mesh = {};
  mesh.primitive = primitive;
  mesh.indexType =
    GL_UNSIGNED_INT; // TODO: Use better index type if numVertices allowes it.
  mesh.numIndices = numIndices;
  mesh.numVertices = numVertices;

  glGenVertexArrays( 1, &mesh.vao );
  glBindVertexArray( mesh.vao );

  glGenBuffers( 1, &mesh.vbo );
  if ( mesh.numIndices )
  {
    glGenBuffers( 1, &mesh.ibo );

    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, mesh.ibo );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof( uint32_t ) * numIndices,
        indices, GL_STATIC_DRAW );
  }

  glBindBuffer( GL_ARRAY_BUFFER, mesh.vbo );

  glBufferData( GL_ARRAY_BUFFER, vertexSize * numVertices, vertices,
                GL_STATIC_DRAW );

  for ( uint32_t i = 0; i < numVertexAttributes; ++i )
  {
    OpenGLVertexAttribute *attrib = vertexAttributes + i;
    if ( attrib->index >= MAX_VERTEX_ATTRIBUTES )
    {
      OpenGLDeleteStaticMesh( mesh );
      // TODO: Zero memory.
      mesh.numIndices = 0;
      break;
    }
    glEnableVertexAttribArray( attrib->index);

    glVertexAttribPointer( attrib->index, attrib->numComponents,
                           attrib->componentType, attrib->normalized,
                           vertexSize,
                           ConvertUint32ToPointer( attrib->offset ) );
  }

  glBindVertexArray( 0 );
  return mesh;
}

void OpenGLDrawStaticMesh( OpenGLStaticMesh mesh )
{
  glBindVertexArray( mesh.vao );
  if ( mesh.numIndices )
  {
    glDrawElements( mesh.primitive, mesh.numIndices, mesh.indexType, NULL );
  }
  else
  {
    glDrawArrays( mesh.primitive, 0, mesh.numVertices );
  }
}

OpenGLStaticMesh CreateWireframeCube()
{
  // clang-format off
  float vertices[] =
  {
    -0.5f, 0.5f, -0.5f,
    0.5f, 0.5f, -0.5f,
    0.5f, 0.5f, 0.5f,
    -0.5f, 0.5f, 0.5f,

    -0.5f, -0.5f, -0.5f,
    0.5f, -0.5f, -0.5f,
    0.5f, -0.5f, 0.5f,
    -0.5f, -0.5f, 0.5f,
  };

  uint32_t indices[] =
  {
    // Top
    0, 1,
    1, 2,
    2, 3,
    3, 0,

    // Bottom
    4, 5,
    5, 6,
    6, 7,
    7, 4,

    // Back
    0, 4,
    1, 5,

    // Front
    3, 7,
    2, 6
  };

  OpenGLVertexAttribute attrib;
  attrib.index = VERTEX_ATTRIBUTE_POSITION;
  attrib.numComponents = 3;
  attrib.componentType = GL_FLOAT;
  attrib.normalized = GL_FALSE;
  attrib.offset = 0;

  return OpenGLCreateStaticMesh(
    vertices, 8, indices, 24, sizeof( float ) * 3, &attrib, 1, GL_LINES );
}

OpenGLStaticMesh CreateAxisMesh()
{
  // clang-format off
  float vertices[] =
  {
    // X Axis
    0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
    1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,

    // Y Axis
    0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f,
    0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,

    // Z Axis
    0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f,
    0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f
  };
  // clang-format on

  OpenGLVertexAttribute attribs[2];
  attribs[0].index = VERTEX_ATTRIBUTE_POSITION;
  attribs[0].numComponents = 3;
  attribs[0].componentType = GL_FLOAT;
  attribs[0].normalized = GL_FALSE;
  attribs[0].offset = 0;
  attribs[1].index = VERTEX_ATTRIBUTE_COLOUR;
  attribs[1].numComponents = 3;
  attribs[1].componentType = GL_FLOAT;
  attribs[1].normalized = GL_FALSE;
  attribs[1].offset = sizeof( float ) * 3;

  return OpenGLCreateStaticMesh( vertices, 6, nullptr, 0, sizeof( float ) * 6,
                                 attribs, 2, GL_LINES );
}

OpenGLStaticMesh CreateFullscreenQuadMesh()
{
  float vertices[ ] = {
    -1, 1,0,0,1,
    -1,-1,0,0,0,
     1,-1,0,1,0,
     1, 1,0,1,1,
  };

  OpenGLVertexAttribute attribs[2];
  attribs[0].index = VERTEX_ATTRIBUTE_POSITION;
  attribs[0].numComponents = 3;
  attribs[0].componentType = GL_FLOAT;
  attribs[0].normalized = GL_FALSE;
  attribs[0].offset = 0;
  attribs[1].index = VERTEX_ATTRIBUTE_TEXTURE_COORDINATE;
  attribs[1].numComponents = 2;
  attribs[1].componentType = GL_FLOAT;
  attribs[1].normalized = GL_FALSE;
  attribs[1].offset = sizeof( float ) * 3;

  return OpenGLCreateStaticMesh( vertices, 4, nullptr, 0, sizeof( float ) * 5,
                                 attribs, 2, GL_QUADS );
}

OpenGLStaticMesh
  CreatePlaneMesh( float width, float height, float textureScale )
{
  float vertices[ ] = {
    -width, 0, height, 0, 0, 1, 0, height * textureScale,
    width, 0, height, 0, 1, 0, width * textureScale, height * textureScale,
    width, 0, -height, 0, 1, 0, width * textureScale, 0,
    -width, 0, -height, 0, 1, 0, 0, 0
  };

  OpenGLVertexAttribute attribs[3];
  attribs[0].index = VERTEX_ATTRIBUTE_POSITION;
  attribs[0].numComponents = 3;
  attribs[0].componentType = GL_FLOAT;
  attribs[0].normalized = GL_FALSE;
  attribs[0].offset = 0;
  attribs[1].index = VERTEX_ATTRIBUTE_NORMAL;
  attribs[1].numComponents = 3;
  attribs[1].componentType = GL_FLOAT;
  attribs[1].normalized = GL_FALSE;
  attribs[1].offset = sizeof( float ) * 3;
  attribs[2].index = VERTEX_ATTRIBUTE_TEXTURE_COORDINATE;
  attribs[2].numComponents = 2;
  attribs[2].componentType = GL_FLOAT;
  attribs[2].normalized = GL_FALSE;
  attribs[2].offset = sizeof( float ) * 6;

  return OpenGLCreateStaticMesh( vertices, 4, nullptr, 0, sizeof( float ) * 8,
                                 attribs, 3, GL_QUADS );
}

struct OpenGLTextureFormat
{
  int internalFormat, format, type;
};

internal bool OpenGLGetTextureFormat( int pixelFormat, int pixelComponentType,
    OpenGLTextureFormat *result )
{
  switch( pixelComponentType )
  {
  case PIXEL_COMPONENT_TYPE_UINT8:
    result->type = GL_UNSIGNED_BYTE;
    break;
  default:
    return false;
    break;
  }

  switch( pixelFormat )
  {
  case PIXEL_FORMAT_RGB8:
    result->internalFormat = GL_SRGB8;
    result->format = GL_RGB;
    break;
  case PIXEL_FORMAT_RGBA8:
    result->internalFormat = GL_SRGB8_ALPHA8;
    result->format = GL_RGBA;
    break;
  case PIXEL_FORMAT_NORMAL_MAP:
    result->internalFormat = GL_RGB8;
    result->format = GL_RGB;
    break;
  default:
    return false;
    break;
  }

  return true;
}

void OpenGLDeleteTexture( uint32_t texture )
{
  glDeleteTextures( 1, &texture );
}

uint32_t OpenGLCreateTexture( uint32_t width, uint32_t height,
    int pixelFormat, int pixelComponentType, const void *pixels )
{
  GLuint texture;
  glGenTextures( 1, &texture );
  glBindTexture( GL_TEXTURE_2D, texture );

  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );

  OpenGLTextureFormat format;
  if ( OpenGLGetTextureFormat( pixelFormat, pixelComponentType, &format ) )
  {
    glTexImage2D( GL_TEXTURE_2D, 0, format.internalFormat, width, height, 0,
        format.format, format.type, pixels );

    glBindTexture( GL_TEXTURE_2D, INVALID_OPENGL_TEXTURE );
    return texture;
  }
  OpenGLDeleteTexture( texture );
  return INVALID_OPENGL_TEXTURE;
}

OpenGLStaticMesh CreateCubeMesh()
{
    float vertices[] = {
      // Top
      -0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
      0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
      0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,
      -0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f,

      // Bottom
      -0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f,
      0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f,
      0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f,
      -0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f,

      // Back
      -0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f,
      0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f,
      0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,
      -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f,

      // Front
      -0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,
      0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
      0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
      -0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,

      // Left
      -0.5f, 0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
      -0.5f, -0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
      -0.5f, -0.5f, 0.5f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
      -0.5f, 0.5f, 0.5f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f,

      // Right
      0.5f, 0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
      0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
      0.5f, -0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
      0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
    };

    uint32_t indices[] = {
      2, 1, 0,
      0, 3, 2,

      4, 5, 6,
      6, 7, 4,

      8, 9, 10,
      10, 11, 8,

      14, 13, 12,
      12, 15, 14,

      16, 17, 18,
      18, 19, 16,

      22, 21, 20,
      20, 23, 22
    };

    OpenGLVertexAttribute attribs[3];
    attribs[0].index = VERTEX_ATTRIBUTE_POSITION;
    attribs[0].numComponents = 3;
    attribs[0].componentType = GL_FLOAT;
    attribs[0].normalized = GL_FALSE;
    attribs[0].offset = 0;
    attribs[1].index = VERTEX_ATTRIBUTE_NORMAL;
    attribs[1].numComponents = 3;
    attribs[1].componentType = GL_FLOAT;
    attribs[1].normalized = GL_FALSE;
    attribs[1].offset = sizeof( float ) * 3;
    attribs[2].index = VERTEX_ATTRIBUTE_TEXTURE_COORDINATE;
    attribs[2].numComponents = 2;
    attribs[2].componentType = GL_FLOAT;
    attribs[2].normalized = GL_FALSE;
    attribs[2].offset = sizeof( float ) * 6;

    return OpenGLCreateStaticMesh( vertices, 24, indices, 36,
                                   sizeof( float ) * 8, attribs, 3,
                                   GL_TRIANGLES );
}

struct TriangleIndices
{
  uint32_t i,j,k;
};

inline uint32_t AddMidPoint( uint32_t index1, uint32_t index2,
                             std::vector<glm::vec3> *vertices )
{
  const auto &v1 = vertices->at(index1);
  const auto &v2 = vertices->at(index2);
  vertices->push_back( glm::normalize( v1 + v2 ) );
  return vertices->size() - 1;
}
OpenGLStaticMesh CreateIcosahedronMesh( uint32_t tesselationLevel )
{
  std::vector<glm::vec3> vertices;
  std::vector<TriangleIndices> indices;

  float t = ( 1.0f + glm::sqrt( 5.0f ) ) * 0.5f;
  vertices.push_back( glm::normalize( glm::vec3{ -1, t, 0 } ) );
  vertices.push_back( glm::normalize( glm::vec3{ 1, t, 0 } ) );
  vertices.push_back( glm::normalize( glm::vec3{ -1, -t, 0 } ) );
  vertices.push_back( glm::normalize( glm::vec3{ 1, -t, 0 } ) );

  vertices.push_back( glm::normalize( glm::vec3{ 0, -1, t } ) );
  vertices.push_back( glm::normalize( glm::vec3{ 0, 1, t } ) );
  vertices.push_back( glm::normalize( glm::vec3{ 0, -1, -t } ) );
  vertices.push_back( glm::normalize( glm::vec3{ 0, 1, -t } ) );

  vertices.push_back( glm::normalize( glm::vec3{ t, 0, -1 } ) );
  vertices.push_back( glm::normalize( glm::vec3{ t, 0, 1 } ) );
  vertices.push_back( glm::normalize( glm::vec3{ -t, 0, -1 } ) );
  vertices.push_back( glm::normalize( glm::vec3{ -t, 0, 1 } ) );

  indices.push_back( TriangleIndices{ 0, 11, 5 } );
  indices.push_back( TriangleIndices{ 0, 5, 1 } );
  indices.push_back( TriangleIndices{ 0, 1, 7 } );
  indices.push_back( TriangleIndices{ 0, 7, 10 } );
  indices.push_back( TriangleIndices{ 0, 10, 11 } );

  indices.push_back( TriangleIndices{ 1, 5, 9 } );
  indices.push_back( TriangleIndices{ 5, 11, 4 } );
  indices.push_back( TriangleIndices{ 11, 10, 2 } );
  indices.push_back( TriangleIndices{ 10, 7, 6 } );
  indices.push_back( TriangleIndices{ 7, 1, 8 } );

  indices.push_back( TriangleIndices{ 3, 9, 4 } );
  indices.push_back( TriangleIndices{ 3, 4, 2 } );
  indices.push_back( TriangleIndices{ 3, 2, 6 } );
  indices.push_back( TriangleIndices{ 3, 6, 8 } );
  indices.push_back( TriangleIndices{ 3, 8, 9 } );

  indices.push_back( TriangleIndices{ 4, 9, 5 } );
  indices.push_back( TriangleIndices{ 2, 4, 11 } );
  indices.push_back( TriangleIndices{ 6, 2, 10 } );
  indices.push_back( TriangleIndices{ 8, 6, 7 } );
  indices.push_back( TriangleIndices{ 9, 8, 1 } );

  for ( uint32_t i = 0; i < tesselationLevel; ++i )
  {
    std::vector<TriangleIndices> newIndices;
    for ( size_t j = 0; j < indices.size(); ++j )
    {
      uint32_t a = AddMidPoint( indices[j].i, indices[j].j, &vertices );
      uint32_t b = AddMidPoint( indices[j].j, indices[j].k, &vertices );
      uint32_t c = AddMidPoint( indices[j].k, indices[j].i, &vertices );

      newIndices.push_back( TriangleIndices{ indices[j].i, a, c } );
      newIndices.push_back( TriangleIndices{ indices[j].j, b, a } );
      newIndices.push_back( TriangleIndices{ indices[j].k, c, b } );
      newIndices.push_back( TriangleIndices{ a, b, c } );
    }

    indices = newIndices;
  }

  OpenGLVertexAttribute attribs[1];
  attribs[0].index = VERTEX_ATTRIBUTE_POSITION;
  attribs[0].numComponents = 3;
  attribs[0].componentType = GL_FLOAT;
  attribs[0].normalized = GL_FALSE;
  attribs[0].offset = 0;

  return OpenGLCreateStaticMesh(
    vertices.data(), vertices.size(), (uint32_t *)indices.data(),
    indices.size() * 3, sizeof( glm::vec3 ), attribs, 1, GL_TRIANGLES );
}

OpenGLStaticMesh CreateCone( uint32_t segments )
{
  ASSERT( segments >= 3 );
  std::vector<glm::vec3> vertices;
  std::vector<uint32_t> indices;
  vertices.push_back( glm::vec3{} );
  vertices.push_back( glm::vec3{ 0, 0, 1 } );

  float inc = glm::pi<float>() * 2.0f / segments;
  for ( uint32_t i = 0; i < segments; ++i )
  {
    float t = (float)i / segments;
    float angle = t * glm::pi<float>() * 2.0f;
    vertices.push_back( glm::vec3{glm::cos( angle ), glm::sin( angle ), 1.0f } );
    indices.push_back( vertices.size() );
    indices.push_back( 0 );
    indices.push_back( vertices.size() -1 );
    indices.push_back( vertices.size() -1 );
    indices.push_back( 1 );
    indices.push_back( vertices.size() );
  }
  *(indices.end() - 6) = 2;
  *(indices.end() - 1) = 2;

  OpenGLVertexAttribute attribs[1];
  attribs[0].index = VERTEX_ATTRIBUTE_POSITION;
  attribs[0].numComponents = 3;
  attribs[0].componentType = GL_FLOAT;
  attribs[0].normalized = GL_FALSE;
  attribs[0].offset = 0;

  return OpenGLCreateStaticMesh(
    vertices.data(), vertices.size(), indices.data(), indices.size(),
    sizeof( glm::vec3 ), attribs, 1, GL_TRIANGLES );
}

TangentData ComputeTangentData( const glm::vec3 &a, const glm::vec2 &h,
                                const glm::vec3 &b, const glm::vec2 &i,
                                const glm::vec3 &c, const glm::vec2 &j,
                                const glm::vec3 &N )
{
  TangentData result;

#if 0
  glm::mat3x2 mat = glm::inverse( glm::mat2{g.y, -f.y, -g.x, f.x} ) *
                    glm::mat3x2{d.x, d.y, d.z, e.x, e.y, e.z};

  result.tangent = glm::normalize( glm::vec3{mat[0].x, mat[1].x, mat[2].x} );
  result.bitangent = glm::normalize( glm::vec3{mat[0].y, mat[1].y, mat[2].y} );
#endif

  glm::vec3 v = b - a, w = c - a;

  float sx = i.x - h.x, sy = i.y - h.y;
  float tx = j.x - h.x, ty = j.y - h.y;
  float dirCorrection = ( tx * sy - ty * sx ) < 0.0f ? -1.0f : 1.0f;
  if ( 0 == sx && 0 ==sy && 0 == tx && 0 == ty )
  {
    sx = 0.0; sy = 1.0;
    tx = 1.0; ty = 0.0;
  }
  glm::vec3 tangent;
  tangent.x = (w.x * sy - v.x * ty) * dirCorrection;
  tangent.y = (w.y * sy - v.y * ty) * dirCorrection;
  tangent.z = (w.z * sy - v.z * ty) * dirCorrection;

  glm::vec3 localTangent = tangent - N * ( tangent * N );
  localTangent = glm::normalize( localTangent );
  result.tangent = localTangent;

  return result;
}
