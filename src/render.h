#pragma once

#include <cstdint>

struct GBuffer
{
  uint32_t width, height, fbo, albedoTexture, normalTexture, depthTexture,
    surfaceTexture;
};

struct HDRBuffer
{
  uint32_t width, height, fbo, hdrTexture;
};

struct VolumetricBuffer
{
  uint32_t width, height, fbo, depthTexture;
};

struct ShadowBuffer
{
  uint32_t fbo, depthTexture, size;
};

struct RenderPassBuffer
{
  uint32_t size, fbo, texture;
};


extern bool InitializeGBuffer( GBuffer *gbuffer, uint32_t width,
                               uint32_t height );

extern void DeinitializeGBuffer( GBuffer *gbuffer );

extern bool InitializeHDRBuffer( HDRBuffer *buffer, uint32_t width,
                                 uint32_t height );

extern void DeinitializeHDRBuffer( HDRBuffer *buffer );

extern bool InitializeVolumetricBuffer( VolumetricBuffer *buffer,
                                        uint32_t width, uint32_t height );

extern void DeinitializeVolumetricBuffer( VolumetricBuffer *buffer );

extern bool InitializeShadowBuffer( ShadowBuffer *shadowBuffer, uint32_t size );

extern bool InitializeRenderPassBuffer( RenderPassBuffer *buffer,
                                        uint32_t size );
extern void DeinitializeRenderPassBuffer( RenderPassBuffer *buffer,
                                          uint32_t size );
