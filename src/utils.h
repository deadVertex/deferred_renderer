#pragma once

#define ASSERT( EXP ) if( !(EXP) ) { *(int*)0 = 0; }
#define internal static
#define ARRAY_COUNT( ARRAY ) ( sizeof( ARRAY ) / sizeof( ARRAY[0] ) )
#define BIT( N ) ( 1 << N )
